﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class alimentosssController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /alimentosss/

        public ActionResult Index()
        {
            var alimentoes = db.alimentoes.Include(a => a.tipo_alimento);
            return View(alimentoes.ToList());
        }

        //
        // GET: /alimentosss/Details/5

        public ActionResult Details(int id = 0)
        {
            alimento alimento = db.alimentoes.Find(id);
            if (alimento == null)
            {
                return HttpNotFound();
            }
            return View(alimento);
        }

        //
        // GET: /alimentosss/Create

        public ActionResult Create()
        {
            ViewBag.Fk_tipoalimento = new SelectList(db.tipo_alimento, "Id_tipoalimento", "Nombre_tipoalimento");
            return View();
        }

        //
        // POST: /alimentosss/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(alimento alimento)
        {
            if (ModelState.IsValid)
            {
                db.alimentoes.Add(alimento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Fk_tipoalimento = new SelectList(db.tipo_alimento, "Id_tipoalimento", "Nombre_tipoalimento", alimento.Fk_tipoalimento);
            return View(alimento);
        }

        //
        // GET: /alimentosss/Edit/5

        public ActionResult Edit(int id = 0)
        {
            alimento alimento = db.alimentoes.Find(id);
            if (alimento == null)
            {
                return HttpNotFound();
            }
            ViewBag.Fk_tipoalimento = new SelectList(db.tipo_alimento, "Id_tipoalimento", "Nombre_tipoalimento", alimento.Fk_tipoalimento);
            return View(alimento);
        }

        //
        // POST: /alimentosss/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(alimento alimento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alimento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Fk_tipoalimento = new SelectList(db.tipo_alimento, "Id_tipoalimento", "Nombre_tipoalimento", alimento.Fk_tipoalimento);
            return View(alimento);
        }

        //
        // GET: /alimentosss/Delete/5

        public ActionResult Delete(int id = 0)
        {
            alimento alimento = db.alimentoes.Find(id);
            if (alimento == null)
            {
                return HttpNotFound();
            }
            return View(alimento);
        }

        //
        // POST: /alimentosss/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            alimento alimento = db.alimentoes.Find(id);
            db.alimentoes.Remove(alimento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}