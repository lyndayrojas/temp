﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class alimento_bodegaController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /alimento_bodega/

        public ActionResult Index()
        {
            var alimento_bodega = db.alimento_bodega.Include(a => a.alimento).Include(a => a.bodega);
            return View(alimento_bodega.ToList());
        }

        //
        // GET: /alimento_bodega/Details/5

        public ActionResult Details(int id = 0)
        {
            alimento_bodega alimento_bodega = db.alimento_bodega.Find(id);
            if (alimento_bodega == null)
            {
                return HttpNotFound();
            }
            return View(alimento_bodega);
        }

        //
        // GET: /alimento_bodega/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento");
            ViewBag.FK_Id_Bog = new SelectList(db.bodegas, "Id_Bog", "Nom_Bog");
            return View();
        }

        //
        // POST: /alimento_bodega/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(alimento_bodega alimento_bodega)
        {
            if (ModelState.IsValid)
            {
                db.alimento_bodega.Add(alimento_bodega);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", alimento_bodega.FK_Id_Alimento);
            ViewBag.FK_Id_Bog = new SelectList(db.bodegas, "Id_Bog", "Nom_Bog", alimento_bodega.FK_Id_Bog);
            return View(alimento_bodega);
        }

        //
        // GET: /alimento_bodega/Edit/5

        public ActionResult Edit(int id = 0)
        {
            alimento_bodega alimento_bodega = db.alimento_bodega.Find(id);
            if (alimento_bodega == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", alimento_bodega.FK_Id_Alimento);
            ViewBag.FK_Id_Bog = new SelectList(db.bodegas, "Id_Bog", "Nom_Bog", alimento_bodega.FK_Id_Bog);
            return View(alimento_bodega);
        }

        //
        // POST: /alimento_bodega/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(alimento_bodega alimento_bodega)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alimento_bodega).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", alimento_bodega.FK_Id_Alimento);
            ViewBag.FK_Id_Bog = new SelectList(db.bodegas, "Id_Bog", "Nom_Bog", alimento_bodega.FK_Id_Bog);
            return View(alimento_bodega);
        }

        //
        // GET: /alimento_bodega/Delete/5

        public ActionResult Delete(int id = 0)
        {
            alimento_bodega alimento_bodega = db.alimento_bodega.Find(id);
            if (alimento_bodega == null)
            {
                return HttpNotFound();
            }
            return View(alimento_bodega);
        }

        //
        // POST: /alimento_bodega/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            alimento_bodega alimento_bodega = db.alimento_bodega.Find(id);
            db.alimento_bodega.Remove(alimento_bodega);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}