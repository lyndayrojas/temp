﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class alimentController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /aliment/

        public ActionResult Index()
        {
            return View(db.alimen.ToList());
        }

        //
        // GET: /aliment/Details/5

        public ActionResult Details(string id = null)
        {
            aliman aliman = db.alimen.Find(id);
            if (aliman == null)
            {
                return HttpNotFound();
            }
            return View(aliman);
        }

        //
        // GET: /aliment/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /aliment/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(aliman aliman)
        {
            if (ModelState.IsValid)
            {
                db.alimen.Add(aliman);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aliman);
        }

        //
        // GET: /aliment/Edit/5

        public ActionResult Edit(string id = null)
        {
            aliman aliman = db.alimen.Find(id);
            if (aliman == null)
            {
                return HttpNotFound();
            }
            return View(aliman);
        }

        //
        // POST: /aliment/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(aliman aliman)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aliman).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aliman);
        }

        //
        // GET: /aliment/Delete/5

        public ActionResult Delete(string id = null)
        {
            aliman aliman = db.alimen.Find(id);
            if (aliman == null)
            {
                return HttpNotFound();
            }
            return View(aliman);
        }

        //
        // POST: /aliment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            aliman aliman = db.alimen.Find(id);
            db.alimen.Remove(aliman);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}