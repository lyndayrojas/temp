﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class cultivo_pezController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /cultivo_pez/

        public ActionResult Index()
        {
            var cultivo_pez = db.cultivo_pez.Include(c => c.alimento).Include(c => c.granja).Include(c => c.pez1);
            return View(cultivo_pez.ToList());
        }

        //
        // GET: /cultivo_pez/Details/5

        public ActionResult Details(int id = 0)
        {
            cultivo_pez cultivo_pez = db.cultivo_pez.Find(id);
            if (cultivo_pez == null)
            {
                return HttpNotFound();
            }
            return View(cultivo_pez);
        }

        //
        // GET: /cultivo_pez/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento");
            ViewBag.FK_Id_Granja = new SelectList(db.granjas, "Id_Granja", "Nom_Granja");
            ViewBag.FK_Id_Pez = new SelectList(db.pezs, "Id_Pez", "Nom_Cienti");
            return View();
        }

        //
        // POST: /cultivo_pez/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(cultivo_pez cultivo_pez)
        {
            if (ModelState.IsValid)
            {
                db.cultivo_pez.Add(cultivo_pez);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", cultivo_pez.FK_Id_Alimento);
            ViewBag.FK_Id_Granja = new SelectList(db.granjas, "Id_Granja", "Nom_Granja", cultivo_pez.FK_Id_Granja);
            ViewBag.FK_Id_Pez = new SelectList(db.pezs, "Id_Pez", "Nom_Cienti", cultivo_pez.FK_Id_Pez);
            return View(cultivo_pez);
        }

        //
        // GET: /cultivo_pez/Edit/5

        public ActionResult Edit(int id = 0)
        {
            cultivo_pez cultivo_pez = db.cultivo_pez.Find(id);
            if (cultivo_pez == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", cultivo_pez.FK_Id_Alimento);
            ViewBag.FK_Id_Granja = new SelectList(db.granjas, "Id_Granja", "Nom_Granja", cultivo_pez.FK_Id_Granja);
            ViewBag.FK_Id_Pez = new SelectList(db.pezs, "Id_Pez", "Nom_Cienti", cultivo_pez.FK_Id_Pez);
            return View(cultivo_pez);
        }

        //
        // POST: /cultivo_pez/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(cultivo_pez cultivo_pez)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cultivo_pez).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", cultivo_pez.FK_Id_Alimento);
            ViewBag.FK_Id_Granja = new SelectList(db.granjas, "Id_Granja", "Nom_Granja", cultivo_pez.FK_Id_Granja);
            ViewBag.FK_Id_Pez = new SelectList(db.pezs, "Id_Pez", "Nom_Cienti", cultivo_pez.FK_Id_Pez);
            return View(cultivo_pez);
        }

        //
        // GET: /cultivo_pez/Delete/5

        public ActionResult Delete(int id = 0)
        {
            cultivo_pez cultivo_pez = db.cultivo_pez.Find(id);
            if (cultivo_pez == null)
            {
                return HttpNotFound();
            }
            return View(cultivo_pez);
        }

        //
        // POST: /cultivo_pez/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cultivo_pez cultivo_pez = db.cultivo_pez.Find(id);
            db.cultivo_pez.Remove(cultivo_pez);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}