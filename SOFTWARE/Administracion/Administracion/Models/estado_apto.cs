//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Administracion.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class estado_apto
    {
        public estado_apto()
        {
            this.apartamentos = new HashSet<apartamento>();
        }
    
        public int Id_EstadoApto { get; set; }
        public Nullable<bool> EstadoApto { get; set; }
    
        public virtual ICollection<apartamento> apartamentos { get; set; }
    }
}
