﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class ventaController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /venta/

        public ActionResult Index()
        {
            var ventas = db.ventas.Include(v => v.cliente);
            return View(ventas.ToList());
        }

        //
        // GET: /venta/Details/5

        public ActionResult Details(int id = 0)
        {
            venta venta = db.ventas.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        //
        // GET: /venta/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Cliente = new SelectList(db.clientes, "Id_Cliente", "Nombre_Cliente");
            return View();
        }

        //
        // POST: /venta/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(venta venta)
        {
            if (ModelState.IsValid)
            {
                db.ventas.Add(venta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Cliente = new SelectList(db.clientes, "Id_Cliente", "Nombre_Cliente", venta.FK_Id_Cliente);
            return View(venta);
        }

        //
        // GET: /venta/Edit/5

        public ActionResult Edit(int id = 0)
        {
            venta venta = db.ventas.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Cliente = new SelectList(db.clientes, "Id_Cliente", "Nombre_Cliente", venta.FK_Id_Cliente);
            return View(venta);
        }

        //
        // POST: /venta/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(venta venta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(venta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Cliente = new SelectList(db.clientes, "Id_Cliente", "Nombre_Cliente", venta.FK_Id_Cliente);
            return View(venta);
        }

        //
        // GET: /venta/Delete/5

        public ActionResult Delete(int id = 0)
        {
            venta venta = db.ventas.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        //
        // POST: /venta/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            venta venta = db.ventas.Find(id);
            db.ventas.Remove(venta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}