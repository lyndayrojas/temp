﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class granjaController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /granja/

        public ActionResult Index()
        {
            var granjas = db.granjas.Include(g => g.usuario).Include(g => g.municipio);
            return View(granjas.ToList());
        }

        //
        // GET: /granja/Details/5

        public ActionResult Details(int id = 0)
        {
            granja granja = db.granjas.Find(id);
            if (granja == null)
            {
                return HttpNotFound();
            }
            return View(granja);
        }

        //
        // GET: /granja/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario");
            ViewBag.FK_Id_Muni = new SelectList(db.municipios, "Id_Muni", "Nom_Muni");
            return View();
        }

        //
        // POST: /granja/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(granja granja)
        {
            if (ModelState.IsValid)
            {
                db.granjas.Add(granja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", granja.FK_Id_Usuario);
            ViewBag.FK_Id_Muni = new SelectList(db.municipios, "Id_Muni", "Nom_Muni", granja.FK_Id_Muni);
            return View(granja);
        }

        //
        // GET: /granja/Edit/5

        public ActionResult Edit(int id = 0)
        {
            granja granja = db.granjas.Find(id);
            if (granja == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", granja.FK_Id_Usuario);
            ViewBag.FK_Id_Muni = new SelectList(db.municipios, "Id_Muni", "Nom_Muni", granja.FK_Id_Muni);
            return View(granja);
        }

        //
        // POST: /granja/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(granja granja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(granja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", granja.FK_Id_Usuario);
            ViewBag.FK_Id_Muni = new SelectList(db.municipios, "Id_Muni", "Nom_Muni", granja.FK_Id_Muni);
            return View(granja);
        }

        //
        // GET: /granja/Delete/5

        public ActionResult Delete(int id = 0)
        {
            granja granja = db.granjas.Find(id);
            if (granja == null)
            {
                return HttpNotFound();
            }
            return View(granja);
        }

        //
        // POST: /granja/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            granja granja = db.granjas.Find(id);
            db.granjas.Remove(granja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}