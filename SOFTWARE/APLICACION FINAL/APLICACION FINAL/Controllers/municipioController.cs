﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class municipioController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /municipio/

        public ActionResult Index()
        {
            var municipios = db.municipios.Include(m => m.departamento);
            return View(municipios.ToList());
        }

        //
        // GET: /municipio/Details/5

        public ActionResult Details(int id = 0)
        {
            municipio municipio = db.municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            return View(municipio);
        }

        //
        // GET: /municipio/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Depar = new SelectList(db.departamentoes, "Id_Depar", "Nom_Depar");
            return View();
        }

        //
        // POST: /municipio/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(municipio municipio)
        {
            if (ModelState.IsValid)
            {
                db.municipios.Add(municipio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Depar = new SelectList(db.departamentoes, "Id_Depar", "Nom_Depar", municipio.FK_Id_Depar);
            return View(municipio);
        }

        //
        // GET: /municipio/Edit/5

        public ActionResult Edit(int id = 0)
        {
            municipio municipio = db.municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Depar = new SelectList(db.departamentoes, "Id_Depar", "Nom_Depar", municipio.FK_Id_Depar);
            return View(municipio);
        }

        //
        // POST: /municipio/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(municipio municipio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(municipio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Depar = new SelectList(db.departamentoes, "Id_Depar", "Nom_Depar", municipio.FK_Id_Depar);
            return View(municipio);
        }

        //
        // GET: /municipio/Delete/5

        public ActionResult Delete(int id = 0)
        {
            municipio municipio = db.municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            return View(municipio);
        }

        //
        // POST: /municipio/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            municipio municipio = db.municipios.Find(id);
            db.municipios.Remove(municipio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}