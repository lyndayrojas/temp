﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class pezController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /pez/

        public ActionResult Index()
        {
            return View(db.pezs.ToList());
        }

        //
        // GET: /pez/Details/5

        public ActionResult Details(int id = 0)
        {
            pez pez = db.pezs.Find(id);
            if (pez == null)
            {
                return HttpNotFound();
            }
            return View(pez);
        }

        //
        // GET: /pez/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /pez/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(pez pez)
        {
            if (ModelState.IsValid)
            {
                db.pezs.Add(pez);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pez);
        }

        //
        // GET: /pez/Edit/5

        public ActionResult Edit(int id = 0)
        {
            pez pez = db.pezs.Find(id);
            if (pez == null)
            {
                return HttpNotFound();
            }
            return View(pez);
        }

        //
        // POST: /pez/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(pez pez)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pez).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pez);
        }

        //
        // GET: /pez/Delete/5

        public ActionResult Delete(int id = 0)
        {
            pez pez = db.pezs.Find(id);
            if (pez == null)
            {
                return HttpNotFound();
            }
            return View(pez);
        }

        //
        // POST: /pez/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            pez pez = db.pezs.Find(id);
            db.pezs.Remove(pez);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}