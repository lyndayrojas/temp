﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class alimento_compradoController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /alimento_comprado/

        public ActionResult Index()
        {
            return View(db.alimento_comprado.ToList());
        }

        //
        // GET: /alimento_comprado/Details/5

        public ActionResult Details(string id = null)
        {
            alimento_comprado alimento_comprado = db.alimento_comprado.Find(id);
            if (alimento_comprado == null)
            {
                return HttpNotFound();
            }
            return View(alimento_comprado);
        }

        //
        // GET: /alimento_comprado/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /alimento_comprado/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(alimento_comprado alimento_comprado)
        {
            if (ModelState.IsValid)
            {
                db.alimento_comprado.Add(alimento_comprado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(alimento_comprado);
        }

        //
        // GET: /alimento_comprado/Edit/5

        public ActionResult Edit(string id = null)
        {
            alimento_comprado alimento_comprado = db.alimento_comprado.Find(id);
            if (alimento_comprado == null)
            {
                return HttpNotFound();
            }
            return View(alimento_comprado);
        }

        //
        // POST: /alimento_comprado/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(alimento_comprado alimento_comprado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alimento_comprado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(alimento_comprado);
        }

        //
        // GET: /alimento_comprado/Delete/5

        public ActionResult Delete(string id = null)
        {
            alimento_comprado alimento_comprado = db.alimento_comprado.Find(id);
            if (alimento_comprado == null)
            {
                return HttpNotFound();
            }
            return View(alimento_comprado);
        }

        //
        // POST: /alimento_comprado/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            alimento_comprado alimento_comprado = db.alimento_comprado.Find(id);
            db.alimento_comprado.Remove(alimento_comprado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}