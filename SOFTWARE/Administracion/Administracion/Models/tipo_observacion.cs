//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Administracion.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tipo_observacion
    {
        public tipo_observacion()
        {
            this.observaciones = new HashSet<observacione>();
        }
    
        public int Id_TipoObser { get; set; }
        public string TipoObser { get; set; }
    
        public virtual ICollection<observacione> observaciones { get; set; }
    }
}
