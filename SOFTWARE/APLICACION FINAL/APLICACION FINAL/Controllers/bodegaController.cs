﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class bodegaController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /bodega/

        public ActionResult Index()
        {
            return View(db.bodegas.ToList());
        }

        //
        // GET: /bodega/Details/5

        public ActionResult Details(int id = 0)
        {
            bodega bodega = db.bodegas.Find(id);
            if (bodega == null)
            {
                return HttpNotFound();
            }
            return View(bodega);
        }

        //
        // GET: /bodega/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /bodega/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(bodega bodega)
        {
            if (ModelState.IsValid)
            {
                db.bodegas.Add(bodega);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bodega);
        }

        //
        // GET: /bodega/Edit/5

        public ActionResult Edit(int id = 0)
        {
            bodega bodega = db.bodegas.Find(id);
            if (bodega == null)
            {
                return HttpNotFound();
            }
            return View(bodega);
        }

        //
        // POST: /bodega/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(bodega bodega)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bodega).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bodega);
        }

        //
        // GET: /bodega/Delete/5

        public ActionResult Delete(int id = 0)
        {
            bodega bodega = db.bodegas.Find(id);
            if (bodega == null)
            {
                return HttpNotFound();
            }
            return View(bodega);
        }

        //
        // POST: /bodega/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bodega bodega = db.bodegas.Find(id);
            db.bodegas.Remove(bodega);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}