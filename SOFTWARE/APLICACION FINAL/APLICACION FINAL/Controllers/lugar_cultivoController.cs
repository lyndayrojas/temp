﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class lugar_cultivoController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /lugar_cultivo/

        public ActionResult Index()
        {
            var lugar_cultivo = db.lugar_cultivo.Include(l => l.cultivo_pez);
            return View(lugar_cultivo.ToList());
        }

        //
        // GET: /lugar_cultivo/Details/5

        public ActionResult Details(int id = 0)
        {
            lugar_cultivo lugar_cultivo = db.lugar_cultivo.Find(id);
            if (lugar_cultivo == null)
            {
                return HttpNotFound();
            }
            return View(lugar_cultivo);
        }

        //
        // GET: /lugar_cultivo/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez");
            return View();
        }

        //
        // POST: /lugar_cultivo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(lugar_cultivo lugar_cultivo)
        {
            if (ModelState.IsValid)
            {
                db.lugar_cultivo.Add(lugar_cultivo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", lugar_cultivo.FK_Id_Cultivo_Pez);
            return View(lugar_cultivo);
        }

        //
        // GET: /lugar_cultivo/Edit/5

        public ActionResult Edit(int id = 0)
        {
            lugar_cultivo lugar_cultivo = db.lugar_cultivo.Find(id);
            if (lugar_cultivo == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", lugar_cultivo.FK_Id_Cultivo_Pez);
            return View(lugar_cultivo);
        }

        //
        // POST: /lugar_cultivo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(lugar_cultivo lugar_cultivo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lugar_cultivo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", lugar_cultivo.FK_Id_Cultivo_Pez);
            return View(lugar_cultivo);
        }

        //
        // GET: /lugar_cultivo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            lugar_cultivo lugar_cultivo = db.lugar_cultivo.Find(id);
            if (lugar_cultivo == null)
            {
                return HttpNotFound();
            }
            return View(lugar_cultivo);
        }

        //
        // POST: /lugar_cultivo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            lugar_cultivo lugar_cultivo = db.lugar_cultivo.Find(id);
            db.lugar_cultivo.Remove(lugar_cultivo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}