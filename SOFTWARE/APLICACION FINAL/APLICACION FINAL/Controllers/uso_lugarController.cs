﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class uso_lugarController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /uso_lugar/

        public ActionResult Index()
        {
            return View(db.uso_lugar.ToList());
        }

        //
        // GET: /uso_lugar/Details/5

        public ActionResult Details(int id = 0)
        {
            uso_lugar uso_lugar = db.uso_lugar.Find(id);
            if (uso_lugar == null)
            {
                return HttpNotFound();
            }
            return View(uso_lugar);
        }

        //
        // GET: /uso_lugar/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /uso_lugar/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(uso_lugar uso_lugar)
        {
            if (ModelState.IsValid)
            {
                db.uso_lugar.Add(uso_lugar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uso_lugar);
        }

        //
        // GET: /uso_lugar/Edit/5

        public ActionResult Edit(int id = 0)
        {
            uso_lugar uso_lugar = db.uso_lugar.Find(id);
            if (uso_lugar == null)
            {
                return HttpNotFound();
            }
            return View(uso_lugar);
        }

        //
        // POST: /uso_lugar/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(uso_lugar uso_lugar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uso_lugar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uso_lugar);
        }

        //
        // GET: /uso_lugar/Delete/5

        public ActionResult Delete(int id = 0)
        {
            uso_lugar uso_lugar = db.uso_lugar.Find(id);
            if (uso_lugar == null)
            {
                return HttpNotFound();
            }
            return View(uso_lugar);
        }

        //
        // POST: /uso_lugar/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            uso_lugar uso_lugar = db.uso_lugar.Find(id);
            db.uso_lugar.Remove(uso_lugar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}