﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class departamentoController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /departamento/

        public ActionResult Index()
        {
            return View(db.departamentoes.ToList());
        }

        //
        // GET: /departamento/Details/5

        public ActionResult Details(int id = 0)
        {
            departamento departamento = db.departamentoes.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        //
        // GET: /departamento/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /departamento/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(departamento departamento)
        {
            if (ModelState.IsValid)
            {
                db.departamentoes.Add(departamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(departamento);
        }

        //
        // GET: /departamento/Edit/5

        public ActionResult Edit(int id = 0)
        {
            departamento departamento = db.departamentoes.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        //
        // POST: /departamento/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(departamento departamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(departamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(departamento);
        }

        //
        // GET: /departamento/Delete/5

        public ActionResult Delete(int id = 0)
        {
            departamento departamento = db.departamentoes.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        //
        // POST: /departamento/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            departamento departamento = db.departamentoes.Find(id);
            db.departamentoes.Remove(departamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}