﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class lugar_pecesController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /lugar_peces/

        public ActionResult Index()
        {
            return View(db.lugar_peces.ToList());
        }

        //
        // GET: /lugar_peces/Details/5

        public ActionResult Details(string id = null)
        {
            lugar_peces lugar_peces = db.lugar_peces.Find(id);
            if (lugar_peces == null)
            {
                return HttpNotFound();
            }
            return View(lugar_peces);
        }

        //
        // GET: /lugar_peces/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /lugar_peces/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(lugar_peces lugar_peces)
        {
            if (ModelState.IsValid)
            {
                db.lugar_peces.Add(lugar_peces);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lugar_peces);
        }

        //
        // GET: /lugar_peces/Edit/5

        public ActionResult Edit(string id = null)
        {
            lugar_peces lugar_peces = db.lugar_peces.Find(id);
            if (lugar_peces == null)
            {
                return HttpNotFound();
            }
            return View(lugar_peces);
        }

        //
        // POST: /lugar_peces/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(lugar_peces lugar_peces)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lugar_peces).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lugar_peces);
        }

        //
        // GET: /lugar_peces/Delete/5

        public ActionResult Delete(string id = null)
        {
            lugar_peces lugar_peces = db.lugar_peces.Find(id);
            if (lugar_peces == null)
            {
                return HttpNotFound();
            }
            return View(lugar_peces);
        }

        //
        // POST: /lugar_peces/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            lugar_peces lugar_peces = db.lugar_peces.Find(id);
            db.lugar_peces.Remove(lugar_peces);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}