﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class lugarController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /lugar/

        public ActionResult Index()
        {
            var lugars = db.lugars.Include(l => l.uso_lugar).Include(l => l.tipo_lugar);
            return View(lugars.ToList());
        }

        //
        // GET: /lugar/Details/5

        public ActionResult Details(int id = 0)
        {
            lugar lugar = db.lugars.Find(id);
            if (lugar == null)
            {
                return HttpNotFound();
            }
            return View(lugar);
        }

        //
        // GET: /lugar/Create

        public ActionResult Create()
        {
            ViewBag.Fk_uso_lugar = new SelectList(db.uso_lugar, "Id_Us", "Nombre_Uso");
            ViewBag.Fk_tipo_lugar = new SelectList(db.tipo_lugar, "Id_tipoLugar", "Nombre_tipolugar");
            return View();
        }

        //
        // POST: /lugar/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(lugar lugar)
        {
            if (ModelState.IsValid)
            {
                db.lugars.Add(lugar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Fk_uso_lugar = new SelectList(db.uso_lugar, "Id_Us", "Nombre_Uso", lugar.Fk_uso_lugar);
            ViewBag.Fk_tipo_lugar = new SelectList(db.tipo_lugar, "Id_tipoLugar", "Nombre_tipolugar", lugar.Fk_tipo_lugar);
            return View(lugar);
        }

        //
        // GET: /lugar/Edit/5

        public ActionResult Edit(int id = 0)
        {
            lugar lugar = db.lugars.Find(id);
            if (lugar == null)
            {
                return HttpNotFound();
            }
            ViewBag.Fk_uso_lugar = new SelectList(db.uso_lugar, "Id_Us", "Nombre_Uso", lugar.Fk_uso_lugar);
            ViewBag.Fk_tipo_lugar = new SelectList(db.tipo_lugar, "Id_tipoLugar", "Nombre_tipolugar", lugar.Fk_tipo_lugar);
            return View(lugar);
        }

        //
        // POST: /lugar/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(lugar lugar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lugar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Fk_uso_lugar = new SelectList(db.uso_lugar, "Id_Us", "Nombre_Uso", lugar.Fk_uso_lugar);
            ViewBag.Fk_tipo_lugar = new SelectList(db.tipo_lugar, "Id_tipoLugar", "Nombre_tipolugar", lugar.Fk_tipo_lugar);
            return View(lugar);
        }

        //
        // GET: /lugar/Delete/5

        public ActionResult Delete(int id = 0)
        {
            lugar lugar = db.lugars.Find(id);
            if (lugar == null)
            {
                return HttpNotFound();
            }
            return View(lugar);
        }

        //
        // POST: /lugar/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            lugar lugar = db.lugars.Find(id);
            db.lugars.Remove(lugar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}