﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class compraController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /compra/

        public ActionResult Index()
        {
            var compras = db.compras.Include(c => c.proveedor).Include(c => c.usuario);
            return View(compras.ToList());
        }

        //
        // GET: /compra/Details/5

        public ActionResult Details(int id = 0)
        {
            compra compra = db.compras.Find(id);
            if (compra == null)
            {
                return HttpNotFound();
            }
            return View(compra);
        }

        //
        // GET: /compra/Create

        public ActionResult Create()
        {
            ViewBag.FK_Cedula_Proveedor = new SelectList(db.proveedors, "Cedula_Proveedor", "Nombre1_Proveedor");
            ViewBag.FK_Cedula_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario");
            return View();
        }

        //
        // POST: /compra/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(compra compra)
        {
            if (ModelState.IsValid)
            {
                db.compras.Add(compra);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Cedula_Proveedor = new SelectList(db.proveedors, "Cedula_Proveedor", "Nombre1_Proveedor", compra.FK_Cedula_Proveedor);
            ViewBag.FK_Cedula_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", compra.FK_Cedula_Usuario);
            return View(compra);
        }

        //
        // GET: /compra/Edit/5

        public ActionResult Edit(int id = 0)
        {
            compra compra = db.compras.Find(id);
            if (compra == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Cedula_Proveedor = new SelectList(db.proveedors, "Cedula_Proveedor", "Nombre1_Proveedor", compra.FK_Cedula_Proveedor);
            ViewBag.FK_Cedula_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", compra.FK_Cedula_Usuario);
            return View(compra);
        }

        //
        // POST: /compra/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(compra compra)
        {
            if (ModelState.IsValid)
            {
                db.Entry(compra).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Cedula_Proveedor = new SelectList(db.proveedors, "Cedula_Proveedor", "Nombre1_Proveedor", compra.FK_Cedula_Proveedor);
            ViewBag.FK_Cedula_Usuario = new SelectList(db.usuarios, "Cedula_Usuario", "Nombre_Usuario", compra.FK_Cedula_Usuario);
            return View(compra);
        }

        //
        // GET: /compra/Delete/5

        public ActionResult Delete(int id = 0)
        {
            compra compra = db.compras.Find(id);
            if (compra == null)
            {
                return HttpNotFound();
            }
            return View(compra);
        }

        //
        // POST: /compra/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            compra compra = db.compras.Find(id);
            db.compras.Remove(compra);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}