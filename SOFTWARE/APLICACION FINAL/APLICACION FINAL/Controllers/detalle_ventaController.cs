﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class detalle_ventaController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /detalle_venta/

        public ActionResult Index()
        {
            var detalle_venta = db.detalle_venta.Include(d => d.cultivo_pez).Include(d => d.venta);
            return View(detalle_venta.ToList());
        }

        //
        // GET: /detalle_venta/Details/5

        public ActionResult Details(int id = 0)
        {
            detalle_venta detalle_venta = db.detalle_venta.Find(id);
            if (detalle_venta == null)
            {
                return HttpNotFound();
            }
            return View(detalle_venta);
        }

        //
        // GET: /detalle_venta/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez");
            ViewBag.FK_Id_Venta = new SelectList(db.ventas, "Id_Venta", "Id_Venta");
            return View();
        }

        //
        // POST: /detalle_venta/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(detalle_venta detalle_venta)
        {
            if (ModelState.IsValid)
            {
                db.detalle_venta.Add(detalle_venta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", detalle_venta.FK_Id_Cultivo_Pez);
            ViewBag.FK_Id_Venta = new SelectList(db.ventas, "Id_Venta", "Id_Venta", detalle_venta.FK_Id_Venta);
            return View(detalle_venta);
        }

        //
        // GET: /detalle_venta/Edit/5

        public ActionResult Edit(int id = 0)
        {
            detalle_venta detalle_venta = db.detalle_venta.Find(id);
            if (detalle_venta == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", detalle_venta.FK_Id_Cultivo_Pez);
            ViewBag.FK_Id_Venta = new SelectList(db.ventas, "Id_Venta", "Id_Venta", detalle_venta.FK_Id_Venta);
            return View(detalle_venta);
        }

        //
        // POST: /detalle_venta/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(detalle_venta detalle_venta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalle_venta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Cultivo_Pez = new SelectList(db.cultivo_pez, "Id_Cultivo_Pez", "Id_Cultivo_Pez", detalle_venta.FK_Id_Cultivo_Pez);
            ViewBag.FK_Id_Venta = new SelectList(db.ventas, "Id_Venta", "Id_Venta", detalle_venta.FK_Id_Venta);
            return View(detalle_venta);
        }

        //
        // GET: /detalle_venta/Delete/5

        public ActionResult Delete(int id = 0)
        {
            detalle_venta detalle_venta = db.detalle_venta.Find(id);
            if (detalle_venta == null)
            {
                return HttpNotFound();
            }
            return View(detalle_venta);
        }

        //
        // POST: /detalle_venta/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            detalle_venta detalle_venta = db.detalle_venta.Find(id);
            db.detalle_venta.Remove(detalle_venta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}