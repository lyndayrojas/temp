﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class tipo_alimentoController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /tipo_alimento/

        public ActionResult Index()
        {
            return View(db.tipo_alimento.ToList());
        }

        //
        // GET: /tipo_alimento/Details/5

        public ActionResult Details(int id = 0)
        {
            tipo_alimento tipo_alimento = db.tipo_alimento.Find(id);
            if (tipo_alimento == null)
            {
                return HttpNotFound();
            }
            return View(tipo_alimento);
        }

        //
        // GET: /tipo_alimento/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /tipo_alimento/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tipo_alimento tipo_alimento)
        {
            if (ModelState.IsValid)
            {
                db.tipo_alimento.Add(tipo_alimento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipo_alimento);
        }

        //
        // GET: /tipo_alimento/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tipo_alimento tipo_alimento = db.tipo_alimento.Find(id);
            if (tipo_alimento == null)
            {
                return HttpNotFound();
            }
            return View(tipo_alimento);
        }

        //
        // POST: /tipo_alimento/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tipo_alimento tipo_alimento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_alimento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipo_alimento);
        }

        //
        // GET: /tipo_alimento/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tipo_alimento tipo_alimento = db.tipo_alimento.Find(id);
            if (tipo_alimento == null)
            {
                return HttpNotFound();
            }
            return View(tipo_alimento);
        }

        //
        // POST: /tipo_alimento/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_alimento tipo_alimento = db.tipo_alimento.Find(id);
            db.tipo_alimento.Remove(tipo_alimento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}