﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APLICACION_FINAL.Models;

namespace APLICACION_FINAL.Controllers
{
    public class detalle_compraController : Controller
    {
        private sinpiscEntities db = new sinpiscEntities();

        //
        // GET: /detalle_compra/

        public ActionResult Index()
        {
            var detalle_compra = db.detalle_compra.Include(d => d.alimento).Include(d => d.compra);
            return View(detalle_compra.ToList());
        }

        //
        // GET: /detalle_compra/Details/5

        public ActionResult Details(int id = 0)
        {
            detalle_compra detalle_compra = db.detalle_compra.Find(id);
            if (detalle_compra == null)
            {
                return HttpNotFound();
            }
            return View(detalle_compra);
        }

        //
        // GET: /detalle_compra/Create

        public ActionResult Create()
        {
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento");
            ViewBag.FK_Id_fac_co = new SelectList(db.compras, "Id_fac_co", "Id_fac_co");
            return View();
        }

        //
        // POST: /detalle_compra/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(detalle_compra detalle_compra)
        {
            if (ModelState.IsValid)
            {
                db.detalle_compra.Add(detalle_compra);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", detalle_compra.FK_Id_Alimento);
            ViewBag.FK_Id_fac_co = new SelectList(db.compras, "Id_fac_co", "Id_fac_co", detalle_compra.FK_Id_fac_co);
            return View(detalle_compra);
        }

        //
        // GET: /detalle_compra/Edit/5

        public ActionResult Edit(int id = 0)
        {
            detalle_compra detalle_compra = db.detalle_compra.Find(id);
            if (detalle_compra == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", detalle_compra.FK_Id_Alimento);
            ViewBag.FK_Id_fac_co = new SelectList(db.compras, "Id_fac_co", "Id_fac_co", detalle_compra.FK_Id_fac_co);
            return View(detalle_compra);
        }

        //
        // POST: /detalle_compra/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(detalle_compra detalle_compra)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalle_compra).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Id_Alimento = new SelectList(db.alimentoes, "Id_Alimento", "Nombre_Alimento", detalle_compra.FK_Id_Alimento);
            ViewBag.FK_Id_fac_co = new SelectList(db.compras, "Id_fac_co", "Id_fac_co", detalle_compra.FK_Id_fac_co);
            return View(detalle_compra);
        }

        //
        // GET: /detalle_compra/Delete/5

        public ActionResult Delete(int id = 0)
        {
            detalle_compra detalle_compra = db.detalle_compra.Find(id);
            if (detalle_compra == null)
            {
                return HttpNotFound();
            }
            return View(detalle_compra);
        }

        //
        // POST: /detalle_compra/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            detalle_compra detalle_compra = db.detalle_compra.Find(id);
            db.detalle_compra.Remove(detalle_compra);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}